#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb  6 10:09:48 2019

@author: nithishreddy
"""

import argparse



parser = argparse.ArgumentParser()
parser = argparse.ArgumentParser(description='Description of your program')
parser.add_argument('-f','--foo', help='Description for foo argument', default=[1,2,3])
parser.add_argument('-b','--bar', help='Description for bar argument', required=True)
args = vars(parser.parse_args())


print(args['foo']  , type(args['foo']  ))


if args['foo'] == [1,2,3]:
    # code here
    print('f crt')

if args['bar'] == 'World':
    # code here
    print('b crt')