import numpy as np
import pandas as pd
import sys
sys.path.append('../generic_tools/')
#import backtest as backtest
import empyrical
import datetime



def give_annualized_returns_constant_capital( daily_pnl ):
    '''
    Gives the annualized returns 
    
    daily_pnl -> daily pnl ( capital is always constant )
    
    '''
    
    return sum(daily_pnl)*252/len(daily_pnl)
    



# read the df_prices_nifty here
    



def give_nifty_returns(nifty_df , start_date , end_date):
    df = nifty_df[(nifty_df.datetime>=start_date)&(nifty_df.datetime<=end_date)]
#    benchmark = df_prices_nifty[(df_prices_nifty.datetime>=start_date)& (df_prices_nifty.datetime<=end_date)]
#    benchmark.set_index(["datetime"], inplace=True)
    df["returns"]  = df["Close"].pct_change()
    df = pd.Series(data=df["returns"].values, index=df.datetime.values)
    df.dropna(inplace=True)
    return empyrical.cum_returns_final(df)


def static_capital_leverage(o3,nifty_df):
    '''
    Takes o3 from para_main
    
    returns the returns and capital  deployed for each test period
    '''

    all_returns = []
    cap_dep_list = []
    sharpe_list = []
    hit_rate = []
    trades_list = []
    dates_list = []
    nifty_returns = []
    
    annual_volatility_list = []
    omega_ratio_list = []
    sortino_ratio_list = []
    stability_of_timeseries_list = []
    tail_ratio_list = []
    value_at_risk_list = []
    
    
    for i in range(len(o3)):
        
        x = o3[i]
        
        max_drawdown = x[2]['drawdown']
        max_exposure = x[2]['max_exposure']
        hit_rate_here = x[2]['hit_rate']
        n_trades_here = x[2]['trades']
        df = x[0]
        
        
        start_date  = df.index.min()
        end_date = df.index.max()
        
        date_value = str(start_date)+'-'+str(end_date)
        dates_list.append(date_value)
        df['daily_pnl'] = df['total_pnl'].diff()
        df['daily_trades'] = df['no_of_trades'].diff()
        df = df.iloc[1:]
        df['adj_returns'] = df.daily_pnl - (df.daily_trades*100*0.004) # transaction_cost = 0.004
        
        
        LEVERAGE = 0.5 # variable 
        
        total_capital = 10000000
        single_order_size = 600000
        
        #max_number_of_trades = int(total_capital / single_order_size) 
        
        capital_dep = int(total_capital*LEVERAGE / (single_order_size)) *100 
        # ,  max_exposure*100*LEVERAGE +np.abs(max_drawdown) 
                          
        # we need to exclude drawdown
                          
        returns = df.adj_returns  / capital_dep
        
        
        cum_ret = np.sum(returns)
        all_returns.append(cum_ret)
        
        nifty_ = give_nifty_returns(nifty_df,start_date,end_date)
        nifty_returns.append(nifty_)
        
        hit_rate.append(hit_rate_here)
        
        sharpe_ = empyrical.sharpe_ratio(returns , risk_free=0.1/252)
        sharpe_list.append(sharpe_)
        
        
        annual_volatility = empyrical.annual_volatility(returns)
        annual_volatility_list.append(annual_volatility)
        
        omega_ratio = empyrical.stats.omega_ratio(returns, risk_free=0.1/252 , required_return=0.2/252)
        omega_ratio_list.append(omega_ratio)
        
        sortino_ratio = empyrical.stats.sortino_ratio(returns , 
                                                      required_return=0.2/252, 
                                                      annualization=None, out=None,  
                                                      _downside_risk=None)
        sortino_ratio_list.append(sortino_ratio)
        
        stability_of_timeseries = empyrical.stats.stability_of_timeseries(returns)
        stability_of_timeseries_list.append(stability_of_timeseries)
        
        tail_ratio = empyrical.stats.tail_ratio(returns)
        tail_ratio_list.append(tail_ratio)
        
        value_at_risk = empyrical.stats.value_at_risk(returns, cutoff=0.05)
        value_at_risk_list.append(value_at_risk)
        
        
#        print('start date{0} end date {1} nifty returns{2}'.format(start_date,end_date,nifty_))
        trades_list.append(n_trades_here)
        cap_dep_list.append(capital_dep)
        
        
        
        
        
        
    return dates_list , all_returns, nifty_returns ,sharpe_list , hit_rate , annual_volatility_list, omega_ratio_list,sortino_ratio_list , stability_of_timeseries_list , tail_ratio_list,value_at_risk_list,trades_list,cap_dep_list


def simple_result(o3, nifty_df, file_name):
    dates_list , all_returns, nifty_returns,sharpe_list , hit_rate , annual_volatility_list,omega_ratio_list,sortino_ratio_list , stability_of_timeseries_list , tail_ratio_list,value_at_risk_list,trades_list,cap_dep_list = static_capital_leverage(o3,nifty_df)
    
    print_df = pd.DataFrame({'dates':dates_list,'cum_returns':all_returns, 
                             'nifty_returns':nifty_returns, 
                             'sharpe_ratio':sharpe_list,'hit_rates':hit_rate, 
                             'volatility':annual_volatility_list, 
                             'omega_ratio':omega_ratio_list , 'sortino_ratio':sortino_ratio_list,
                             'stability_of_timeseries':stability_of_timeseries_list,
                             'tail_ratio':tail_ratio_list, 'value_at_risk':value_at_risk_list,
                             'no_of_trades':trades_list})
    print_df.to_excel(file_name,index=False)
    print('result dumped in excel file')
    return None
    




def all_signal_ratios_with_leverage(o3):
    
    df_prices_nifty = pd.read_csv('./data/nifty100.csv')
    df_prices_nifty["datetime"] = df_prices_nifty['Date'].apply(lambda xx:  datetime.datetime.strptime(xx , '%Y-%m-%d' ).date() )
    df_prices_nifty = df_prices_nifty.sort_values(by=['datetime'])
    df_prices_nifty.datetime = pd.to_datetime(df_prices_nifty.datetime)
    df_prices_nifty.datetime = df_prices_nifty.datetime.apply(lambda x: x.date())
    

    output_df = []
    annual_returns = []
    for i in range(len(o3)-1):
        df_daywise_returns = o3[i][0]
        max_drawdown = o3[i][2]['drawdown']
        hit_rate = o3[i][2]['hit_rate']
        no_of_trades = o3[i][2]['trades']
        df1 , df2 = compare_benchmark(df_daywise_returns , max_drawdown,df_prices_nifty)
    #     print(comb_dfs(df1,df2))
        df = comb_dfs(df1,df2)
        i1 = df.index.tolist()
        df = df.append({'model':hit_rate , 'benchmark':np.nan},ignore_index=True)
        df = df.append({'model':no_of_trades , 'benchmark':np.nan},ignore_index=True)
        df.index = i1+['hitrate','trades']
    #     print( ''.join(['=']*40) )
        
        annual_returns.append([df["model"].iloc[0], df["benchmark"].iloc[0]])
        h1  = str(o3[i][0].index.min()) + ' to ' + str(o3[i][0].index.max())
        cols = [(h1,i) for i in df.columns]
        df.columns = pd.MultiIndex.from_tuples(cols)
        output_df.append(df)
    
    output_df = pd.concat(output_df,1)
    
    return output_df

    

def compare_benchmark(df_daywise_returns , max_drawdown , df_prices_nifty):


    returns_strategy = df_daywise_returns[["total_pnl"]]
    returns_strategy["datetime"] = df_daywise_returns.index

    returns_strategy["pnl"] = returns_strategy["total_pnl"] - returns_strategy["total_pnl"].shift(1) ##### the reutns were cumulative, making it single day return
    max_exposure = max(df_daywise_returns["exposure"].values)
    max_drawdown = max_drawdown

    #### the returns in pnl assumed 100 rs order so they represent percentage * 100 / 100 , 
    ###to make it roi of daily basis we will divide by capital deployed 

    returns_strategy["pnl"] = returns_strategy["pnl"]/(100.0*max_exposure + max_drawdown)
#    last_day_strategy = returns_strategy["datetime"].iloc[-1]

    returns_strategy.datetime = pd.to_datetime(returns_strategy.datetime)
    returns_strategy.drop(columns = ['datetime'],inplace=True)
    returns_strategy = pd.Series(returns_strategy.pnl.values , index = returns_strategy.index.values)
    returns_strategy.dropna(inplace=True)

    # start_date and end_date for the benchmark df
    
    start_date = returns_strategy.index.min(); end_date = returns_strategy.index.max()
    print(type(start_date) , type(end_date) , 'types of dates')
    print('benchmark dtypes' , df_prices_nifty.dtypes)
    # benchmark df
    benchmark = df_prices_nifty[(df_prices_nifty.datetime>=start_date)& (df_prices_nifty.datetime<=end_date)]
    benchmark.set_index(["datetime"], inplace=True)
    benchmark["returns"]  = benchmark["Close"].pct_change()
    benchmark = pd.Series(data=benchmark["returns"].values, index=benchmark.index.values)
    benchmark.dropna(inplace=True)


    # calculate alpha and beta

    print( "TEST PERIOD BETWEEN", returns_strategy.index.min()  , returns_strategy.index.max())
    print('benchmark shape' , benchmark.shape)
    
    alpha , beta = empyrical.alpha_beta(returns_strategy, benchmark)

    empyrical_stats = {}
    required_returns = 0.20/252 ### returns promised to investor
    risk_free = 0.07/252
    bench_stats = {}


    empyrical_stats["annual_returns"] = empyrical.stats.annual_return(returns_strategy)
    bench_stats['annual_returns'] = empyrical.stats.annual_return(benchmark)

    empyrical_stats["alpha"] = alpha 
    empyrical_stats["beta"] = beta

    empyrical_stats["max_drawdown"] = empyrical.stats.max_drawdown(returns_strategy)
    bench_stats['max_drawdown'] = empyrical.stats.max_drawdown(benchmark)

    empyrical_stats["annual_volatility"] = empyrical.stats.annual_volatility(returns_strategy)
    bench_stats['annual_volatility'] =  empyrical.stats.annual_volatility(benchmark)

    empyrical_stats["calmar_ratio"] = empyrical.stats.calmar_ratio(returns_strategy)
    bench_stats["calmar_ratio"] = empyrical.stats.calmar_ratio(benchmark)



    empyrical_stats["omega_ratio"] = empyrical.stats.omega_ratio(returns_strategy, risk_free=risk_free, required_return=required_returns)
    bench_stats["omega_ratio"] = empyrical.stats.omega_ratio(benchmark, risk_free=risk_free, required_return=required_returns)



    empyrical_stats["sharpe_ratio"] = empyrical.stats.sharpe_ratio(returns_strategy, risk_free=risk_free)
    bench_stats["sharpe_ratio"] = empyrical.stats.sharpe_ratio(benchmark, risk_free=risk_free)



    empyrical_stats["sortino_ratio"] = empyrical.stats.sortino_ratio(returns_strategy, required_return=required_returns, annualization=None, out=None,_downside_risk=None)
    bench_stats["sortino_ratio"] = empyrical.stats.sortino_ratio(benchmark, required_return=required_returns, annualization=None, out=None,_downside_risk=None)



    empyrical_stats["stability_of_timeseries"] = empyrical.stats.stability_of_timeseries(returns_strategy)
    bench_stats["stability_of_timeseries"] = empyrical.stats.stability_of_timeseries(benchmark)



    empyrical_stats["tail_ratio"] = empyrical.stats.tail_ratio(returns_strategy)
    bench_stats["tail_ratio"] = empyrical.stats.tail_ratio(benchmark)


    empyrical_stats["value_at_risk"] = empyrical.stats.value_at_risk(returns_strategy, cutoff=0.05)
    bench_stats["value_at_risk"] = empyrical.stats.value_at_risk(benchmark, cutoff=0.05)

    empyrical_stats["cagr"] = empyrical.stats.cagr(returns_strategy)
    bench_stats["cagr"] = empyrical.stats.cagr(benchmark)
    
    empyrical_stats["cumulative"] = empyrical.stats.cum_returns_final(returns_strategy, starting_value=0)
    bench_stats["cumulative"] = empyrical.stats.cum_returns_final(benchmark, starting_value=0)

    x1 = pd.DataFrame.from_dict(empyrical_stats, orient='index')
    x2 = pd.DataFrame.from_dict(bench_stats, orient='index')
    
#     print(x1)
#     print(empyrical.stats.aggregate_returns(returns_strategy, convert_to='monthly'))
#     print(empyrical.stats.aggregate_returns(returns_strategy, convert_to='yearly'))

#     print(x2)
#     print(empyrical.stats.aggregate_returns(benchmark, convert_to='monthly'))
#     print(empyrical.stats.aggregate_returns(benchmark, convert_to='yearly'))

    return x1,x2


# ]:
    
def comb_dfs(df1,df2):
    '''
    df1 -> empyrical dict for the model
    df2 -> empyrical dict for the benchmark
    
    return df combined to compare the results
    
    '''
    
    df = pd.merge(df1, df2, left_index=True, right_index=True,how='left')
    df.columns = ['model' , 'benchmark']
    return df

#]
    


def compare_benchmark_all_signals(df_daywise_returns , max_drawdown , df_prices_nifty):


#    returns_strategy = df_daywise_returns[["total_pnl"]]
    returns_strategy = df_daywise_returns[["daily_pnl"]]
    returns_strategy["datetime"] = df_daywise_returns.index

#    returns_strategy["pnl"] = returns_strategy["total_pnl"] - returns_strategy["total_pnl"].shift(1) ##### the reutns were cumulative, making it single day return
    returns_strategy.rename(columns = {'daily_pnl':'pnl'} , inplace=True)
    max_exposure = max(df_daywise_returns["exposure"].values)
    max_drawdown = max_drawdown
    
    #### the returns in pnl assumed 100 rs order so they represent percentage * 100 / 100 , 
    ###to make it roi of daily basis we will divide by capital deployed 

    returns_strategy["pnl"] = returns_strategy["pnl"]/(100.0*max_exposure + max_drawdown)
#    last_day_strategy = returns_strategy["datetime"].iloc[-1]


    returns_strategy = pd.Series(returns_strategy.pnl.values , index = returns_strategy.index.values)
    returns_strategy.dropna(inplace=True)

    # start_date and end_date for the benchmark df
    
    start_date = returns_strategy.index.min(); end_date = returns_strategy.index.max()
    print(type(start_date) , type(end_date) , 'types of dates')
    print('benchmark dtypes' , df_prices_nifty.dtypes)
    # benchmark df
    
    benchmark = df_prices_nifty[(df_prices_nifty.datetime>=start_date)& (df_prices_nifty.datetime<=end_date)]
    benchmark.set_index(["datetime"], inplace=True)
    benchmark["returns"]  = benchmark["Close"].pct_change()
    benchmark = pd.Series(data=benchmark["returns"].values, index=benchmark.index.values)
    benchmark.dropna(inplace=True)


    # calculate alpha and beta

    print( "TEST PERIOD BETWEEN", returns_strategy.index.min()  , returns_strategy.index.max())
    print('benchmark shape' , benchmark.shape)
    
    alpha , beta = empyrical.alpha_beta(returns_strategy, benchmark)
    
    empyrical_stats = {}
    required_returns = 0.20/252 ### returns promised to investor
    risk_free = 0.10/252
    bench_stats = {}


    empyrical_stats["annual_returns"] = empyrical.stats.annual_return(returns_strategy)
    bench_stats['annual_returns'] = empyrical.stats.annual_return(benchmark)

    empyrical_stats["alpha"] = alpha 
    empyrical_stats["beta"] = beta

    empyrical_stats["max_drawdown"] = empyrical.stats.max_drawdown(returns_strategy)
    bench_stats['max_drawdown'] = empyrical.stats.max_drawdown(benchmark)

    empyrical_stats["annual_volatility"] = empyrical.stats.annual_volatility(returns_strategy)
    bench_stats['annual_volatility'] =  empyrical.stats.annual_volatility(benchmark)

    empyrical_stats["calmar_ratio"] = empyrical.stats.calmar_ratio(returns_strategy)
    bench_stats["calmar_ratio"] = empyrical.stats.calmar_ratio(benchmark)



    empyrical_stats["omega_ratio"] = empyrical.stats.omega_ratio(returns_strategy, risk_free=risk_free, required_return=required_returns)
    bench_stats["omega_ratio"] = empyrical.stats.omega_ratio(benchmark, risk_free=risk_free, required_return=required_returns)



    empyrical_stats["sharpe_ratio"] = empyrical.stats.sharpe_ratio(returns_strategy, risk_free=risk_free)
    bench_stats["sharpe_ratio"] = empyrical.stats.sharpe_ratio(benchmark, risk_free=risk_free)



    empyrical_stats["sortino_ratio"] = empyrical.stats.sortino_ratio(returns_strategy, required_return=required_returns, annualization=None, out=None,_downside_risk=None)
    bench_stats["sortino_ratio"] = empyrical.stats.sortino_ratio(benchmark, required_return=required_returns, annualization=None, out=None,_downside_risk=None)



    empyrical_stats["stability_of_timeseries"] = empyrical.stats.stability_of_timeseries(returns_strategy)
    bench_stats["stability_of_timeseries"] = empyrical.stats.stability_of_timeseries(benchmark)



    empyrical_stats["tail_ratio"] = empyrical.stats.tail_ratio(returns_strategy)
    bench_stats["tail_ratio"] = empyrical.stats.tail_ratio(benchmark)


    empyrical_stats["value_at_risk"] = empyrical.stats.value_at_risk(returns_strategy, cutoff=0.05)
    bench_stats["value_at_risk"] = empyrical.stats.value_at_risk(benchmark, cutoff=0.05)

    empyrical_stats["cagr"] = empyrical.stats.cagr(returns_strategy)
    bench_stats["cagr"] = empyrical.stats.cagr(benchmark)
    
    empyrical_stats["cumulative"] = empyrical.stats.cum_returns_final(returns_strategy, starting_value=0)
    bench_stats["cumulative"] = empyrical.stats.cum_returns_final(benchmark, starting_value=0)

    x1 = pd.DataFrame.from_dict(empyrical_stats, orient='index')
    x2 = pd.DataFrame.from_dict(bench_stats, orient='index')
    
#     print(x1)
#     print(empyrical.stats.aggregate_returns(returns_strategy, convert_to='monthly'))
#     print(empyrical.stats.aggregate_returns(returns_strategy, convert_to='yearly'))

#     print(x2)
#     print(empyrical.stats.aggregate_returns(benchmark, convert_to='monthly'))
#     print(empyrical.stats.aggregate_returns(benchmark, convert_to='yearly'))

    return x1,x2
