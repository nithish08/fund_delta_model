# coding: utf-8

# 1]:

import pandas as pd
import numpy as np
import datetime

import sys
sys.path.append('/Users/nithishreddy/workspace/python/generic_tools/')
from collections import Counter
import utils

import pickle
pd.set_option('chained_assignment',None)


import importlib
importlib.reload(utils)


### Data with Ratios and announcements date 


DATE_COL = 'date'
COMPANY_COL = 'nse_symbol'


# 5]:

df = pd.read_csv('~/workspace/python/fund_delta_model/data/final_data/nse_fo/capitaline_nse_po_final.csv')


df.date = pd.to_datetime(df.date)

rem_cols = ['total_of_promoter_and_group','sellingampadministrative_expenses',
            'raw_material_consumed','total_expenditure_minus_raw_material']


cols = [i for i in df.columns if i not in rem_cols]
df = df[cols]


df.drop(columns = ['price_to_earning','price_to_cash_eps' ] ,axis = 1,inplace=True)



df = df.drop_duplicates()
df.replace([np.inf,-np.inf],np.nan,inplace=True)



df.replace(0,np.nan,inplace=True)

importlib.reload(utils)




features_here = [i for i in df.columns if i not in ['nse_symbol','date']]



df_features = utils.create_delta_features(df,'nse_symbol','date',
                                 features=features_here,
                                 delta=[1,2,4],d_delta=[1,2,4])

f1 = df.columns
f2 = df_features.columns
all_features = [i for i in f2 if i not in f1]



## deal with values not in (-300,+300)

# for i in all_features:
#     df_features[i] = df_features[i].apply(lambda x: 300 if x>300 else x)
#     df_features[i] = df_features[i].apply(lambda x: -300 if x<-300 else x)

# reading from file made by make_price_df.py
price_df = pd.read_pickle('./data/data_store/price_df_open_to_open.pkl')

print('these are the companies which dont have price data' )

l1 = df.nse_symbol.unique()
l2 = price_df.nse_symbol.unique()
for i in l1:
    if i not in l2:
        print(i)
        
print('===================================================')


##remove the weekend prices

price_df['weekday'] = price_df.date.apply(lambda x: x.weekday())
week_days_index = [0,1,2,3,4] # weekdays , monday -> friday
print(price_df.shape)
price_df = price_df[price_df.weekday.isin(week_days_index)]
print(price_df.shape)


price_df = price_df[price_df.date.dt.year>2010] #taking data from 2011


df_features = df_features[df_features.date.dt.year>2010] # subsetting the features df

A = price_df.groupby('nse_symbol')['date'].nunique()


max_occ = list(Counter(A).keys())[0]
uniq_comps = A[A==max_occ].index

df_features['reference_date'] = df_features[DATE_COL] + datetime.timedelta(days=2) ## days diff


price_df = price_df[price_df.nse_symbol.isin(uniq_comps)]


df_features = df_features[df_features.nse_symbol.isin(uniq_comps)]

price_df.nse_symbol.nunique() , df_features.nse_symbol.nunique()

total_all = pd.merge_asof(df_features.sort_values('reference_date'), price_df.sort_values(
    'date'), left_on='reference_date', right_on='date', direction='forward', by='nse_symbol')

total_all_with_zero = total_all.copy()
total_all_with_zero[all_features] = total_all_with_zero[all_features].fillna(0)

# store the final dfs in pickle objects to be read for the main file

total_all_with_zero.to_pickle('./data/data_store/total_all_open_to_open.pkl')
# price_df.to_pickle('./data/data_store/price_df_open_to_open.pkl')
with open('./data/data_store/all_features_t2.pkl','wb') as f:
    pickle.dump(all_features,f)



# 8]:

