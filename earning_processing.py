THIS_YEAR = 2018

def from_year(A , time_col='board_meeting',start_year = 2008):
    df = A.copy(deep=True)
    df['year_col'] = df[time_col].apply(lambda x : x.year)
    df['year_col'].fillna(0 , inplace=True)
    # take date starting from 2008
    print(df.shape)
    df = df[df.year_col>=2008]
    print(df.shape)
    df.drop(columns=['year_col'] , inplace=True)
    return df

def min_datapoints(A,col = 'co_name',minpoints=10):
    a = A[col].value_counts()
    a = a[a>=minpoints]
    a = a.index.tolist()
    B = A.copy(deep=True)
    return B[B[col].isin(a)]

def remove_discont(A , time_col='board_meeting',col = 'co_name' , latest_year=THIS_YEAR):
    B = A.copy(deep=True)
    B['year'] = B[time_col].apply(lambda x: x.year)
    B = B[B.year==latest_year]
    l_ = B[col].unique().tolist()
    return A[A[col].isin(l_)]

def rem_tata_rows(row):
    if row['co_name']=='Tata Motors' and row['year']<2010:return False
    else: return True