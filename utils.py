import pandas as pd
import numpy as np
from pandas import ExcelWriter
import os
import pickle
import itertools as it


def make_dict_iterations(d):
    list_of_dicts = [ dict( zip( list(d.keys()) ,i )) for i in list(it.product(*list(d.values())))]
    return list_of_dicts

def pd_xls_read(path):
    xls = pd.ExcelFile(path)
    l = xls.sheet_names
    l = [pd.read_excel(xls,i) for i in l]
    df = pd.concat(l)
    return df

def save_xls(list_dfs,names, xls_path):
    writer = ExcelWriter(xls_path)
    for n, df in zip(names,list_dfs):
        df.to_excel(writer,'sheet%s' % n,index=False)
    writer.save()


def conv_to_npdatetime(df_in , columns = ['board_meeting']):
    df = df_in.copy(deep='True')
    df[columns] = df[columns].applymap(lambda x: np.datetime64(x,'D'))
    return df

def discretize_func(x, threshold=1):
    if np.isnan(x):
        return x
    elif x > threshold: return 1
    elif x < -threshold: return -1
    else : return 0
    
def discretize_3_class_func(x):
    if np.isnan(x):
        return x
    elif x>5: return 1
    elif x<-5: return -1
    else: return 0
    

def disc_2_using_thres(train,threshold):
    train = train/threshold
    train[train>=1]=1
    train[train<1]=0
    return train
    

def disc_3_using_thres(train,threshold):
    train = train / threshold
    train[train>=1] = 1
    train[train<=-1] = -1
    train[(train>-1)&(train<1)] = 0 
    return train
    


def make_target(price_df_in , days,company_col,date_col):
    
    price_df = price_df_in.copy(deep=True)
    price_df.sort_values([company_col,date_col]).reset_index(drop=True,inplace=True)
    
    for j in days:
        price_df['ret_'+str(j)] = price_df.groupby(company_col)['close_price'].shift(-j)
        price_df['ret_'+str(j)] =((price_df['ret_'+str(j)] - price_df['open_price']) \
        / np.abs(price_df['open_price']) ) * 100
        price_df['exit_date'+str(j)] = price_df.groupby(company_col)[date_col].shift(-j)
        
    o2 = ['ret_'+str(i) for i in days]
    return price_df,  o2

def make_target_open(price_df_in , days,company_col,date_col):
    
    price_df = price_df_in.copy(deep=True)
    price_df.sort_values([company_col,date_col]).reset_index(drop=True,inplace=True)
    
    for j in days:
        price_df['ret_'+str(j)] = price_df.groupby(company_col)['open_price'].shift(-j)
        price_df['ret_'+str(j)] =((price_df['ret_'+str(j)] - price_df['open_price']) \
        / np.abs(price_df['open_price']) ) * 100
        price_df['exit_date'+str(j)] = price_df.groupby(company_col)[date_col].shift(-j)
        
    o2 = ['ret_'+str(i) for i in days]
    return price_df,  o2

def make_target_close(price_df_in , days,company_col,date_col):
    
    price_df = price_df_in.copy(deep=True)
    price_df.sort_values([company_col,date_col]).reset_index(drop=True,inplace=True)
    
    for j in days:
        price_df['ret_'+str(j)] = price_df.groupby(company_col)['close_price'].shift(-j)
        price_df['ret_'+str(j)] =((price_df['ret_'+str(j)] - price_df['close_price']) \
        / np.abs(price_df['close_price']) ) * 100
        price_df['exit_date'+str(j)] = price_df.groupby(company_col)[date_col].shift(-j)
        
    o2 = ['ret_'+str(i) for i in days]
    return price_df,  o2





def make_target_kanwal(price_df, days):
    
    new_df = pd.DataFrame([])
    tickers = price_df["co_name"].unique()
    for ticker in tickers:
        ticker_df = price_df[price_df["co_name"] == ticker]
        ticker_df.sort_values('date',inplace=True)
        
        for day in days:    
            ticker_df["ret_"+str(day)] = (ticker_df["close_price"].shift(-day) - ticker_df['open_price'])*100.0 / ticker_df['open_price']
        
        new_df = pd.concat([new_df, ticker_df])
    
    return new_df


def make_price_momentum_features(price_df_in,company_col , date_col , days):
    
    price_df = price_df_in.copy(deep=True)
    price_df.sort_values([company_col,date_col] , inplace=True)
    price_df.reset_index(drop=True,inplace=True)
    
    for j in days:
        price_df['price_mom_'+str(j)] = price_df.groupby(company_col)['close_price'].shift(j)
        price_df['price_mom_'+str(j)] = (price_df['open_price'] - price_df['price_mom_'+str(j)])*100 / np.abs(price_df['price_mom_'+str(j) ])
        
    o2 = ['price_mom_'+str(i) for i in days]
    return price_df,  o2


def make_momentum_features(price_df_in,company_col , date_col,features , days):
    '''
    price_df_in -> dataframe on which we need to calculate momentum features
                    not necessarily prices df
    company_col , date_col -> names of company col and date_col
    
    
    '''
    price_df = price_df_in.copy(deep=True)
    price_df.sort_values([company_col,date_col],inplace=True)
    price_df.reset_index(drop=True,inplace=True)
    
    all_new_features_names = []
    for j in days:
        new_features = [f +'_mom_'+str(j) for f in features]
#         print(len(new_features) , len(features) ) 
        for i in new_features:
            price_df[i] = np.nan
#         print(new_features , features)
#         print(price_df[new_features].shape ) 
#         print(price_df.groupby(company_col)[features].shift(j).shape)
        price_df[new_features] = price_df.groupby(company_col)[features].shift(j)
#         print( price_df[features].shape ) 
#         print(price_df[new_features].shape)
#         print((price_df[features] - price_df[new_features]).shape)
        price_df[new_features] = (price_df[features].values - price_df[new_features].values)*100 /  \
        np.abs(price_df[new_features].values)
        all_new_features_names = all_new_features_names + new_features
    
    return price_df,  all_new_features_names




def create_delta_features(df_in, company_col,time_col, features, delta,d_delta):
    '''
    Input : 

    df_in -> pd.dataframe with co_name and board_meeting(time_col)
    time_col -> time col in df_in used to sort the values to create delta features 
    features -> list of column names in df_in for which we need to create delta features
    delta -> list of delta used to create features

    Output : 

    G -> output pd.df with delta features 
    fl_ -> names of features in the dataframe

    '''
    D = df_in.copy(deep='True')
    
    G = pd.DataFrame()
    diff1_feats = [i+'1' for i in features] 
    
    for i in D[company_col].unique():
        S = D[D[company_col] == i]
        S = S.sort_values(time_col).reset_index(drop=True)
        
        for d_ in delta:
            feat_names = [x_+str(d_) for x_ in features]
            S[feat_names] =  S[features].diff(d_)*100  / np.abs(S[features].shift(d_))
            S[feat_names] = S[feat_names].replace(0,np.nan)
            
        for d_ in d_delta:
            dd_feat_names = [x_ + '_dd'+str(d_) for x_ in features]
            S[dd_feat_names] = (S[diff1_feats].diff(d_)*100/ np.abs(S[diff1_feats].shift(d_)) ) 
        
        G = pd.concat([G,S])
        
    return G


def create_dir(filepath):
    if not os.path.exists(filepath):
        os.mkdir(filepath)

def append_row(filepath,dict_):
    
    if os.path.exists(filepath):
        x = pd.read_csv(filepath,engine='python')
    else:
        x = pd.DataFrame()
    x = x.append(dict_ , ignore_index=True)
    x.to_csv(filepath,index=False)


def dump_dict(filepath , filename , dict_):
    
    with open(os.path.join(filepath , filename+'.pkl') ,'wb') as f:
        pickle.dump(dict_,f)
        
        

def dict_path_to_df(path):
    l = os.listdir(path)
    l = [pd.read_pickle(os.path.join( path , i )) for i in l]
    l = pd.DataFrame(l)
    return l