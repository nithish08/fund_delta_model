
# coding: utf-8

# In[206]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os


# In[207]:


cap_map = pd.read_excel('./data/raw_data/capitaline_capiq_mappings.xls')


# In[208]:


cap_map.head()


# In[209]:


dd_ = dict(cap_map[['col2','CO_NAME']].drop_duplicates().values)


# In[210]:


def pd_xls_read(path):
    xls = pd.ExcelFile(path)
    l = xls.sheet_names
    l = [pd.read_excel(xls,i) for i in l]
    df = pd.concat(l)
    return df


# In[211]:


def read_capiq_fund_data(path1):
    xls = pd.ExcelFile(path1)
    l = xls.sheet_names
    ll_ = []
    s_ =0 
    for s_name in l:
        df = pd.read_excel(xls,s_name)
        val = df.iloc[0,1]
    #     print(s_name)
        if val[:3] =='BSE':
            try:
        #         print(df)
        #         df = pd.DataFrame(df.iloc[3:] , columns=df.iloc[2,:].values.tolist())
                df = pd.read_excel(xls,s_name,skiprows=3)
        #         print(df.shape)
                df.columns = df.columns.str.strip().str.lower().str.replace(' ', '_').str.replace('(', '').str.replace(')', '')
                df['p_by_eps']=  df['price'] / df['eps']
                df['p_by_bv'] = df['price'] / df['bv_share']
                df['mkt_sales'] = df['market_cap'] / df['total_revenue']
                wanted_cols = ['quarterly_earnings_announcement_dates'
                                ,'period'
                                ,'gross_margin_%'
                                ,'earnings_from_cont._ops_margin_%'
                                ,'net_income_margin_%'
                                ,'levered_free_cash_flow_margin_%'
                                ,'unlevered_free_cash_flow_margin_%'
                                ,'ebt_excl_unusual_items','return_on_equity_%' 
                                ,'return_on_common_equity_%'
                                ,"sg&a_margin_%" 
                                ,'total_asset_turnover'
                                ,'fixed_asset_turnover','eps','bv_share','p_by_eps','p_by_bv','mkt_sales']
                df = df[wanted_cols]
                df['bse_ticker'] = val
                df = df[df.quarterly_earnings_announcement_dates.dt.year>=2008]
                ll_.append(df)
                print(df.shape)
                s_+=1
            except:
                print(s_name)
    ll_ = pd.concat(ll_)
    return ll_


# In[213]:

# In[214]:


cap_data['co_name'] = cap_data.bse_ticker.apply(lambda x: dd_[x])


# In[215]:


cap_data.isnull().sum()


# In[110]:


cap_data[cap_data.quarterly_earnings_announcement_dates.dt.year>=2017].shape


# In[248]:


def fill_NM(df_in , columns ):
    df_in.reset_index(inplace=True,drop=True)
    df = df_in.copy(deep=True)
    for col_name in columns:
        
        if df.loc[df[col_name]=='NM',:].shape[0]>0:
            for comp_name in df.loc[df[col_name]=='NM',:]['co_name'].unique():
#                 print( df.loc[ ((df.co_name==comp_name) & (df[col_name=='NM']) ) ,col])
                df.loc[(df.co_name==comp_name) & (df[col_name]=='NM'),col_name] = df.loc[(df.co_name==comp_name) & (df[col_name]!='NM'),col_name].mean()
    return df
            


# In[224]:


# In[ ]:



# In[249]:


df = fill_NM(cap_data,['return_on_common_equity_%'])


# In[254]:

# In[255]:


df = df.dropna()


# In[257]:


df.to_excel('./data/final_data/cap_fin_data.xlsx',index=False)


