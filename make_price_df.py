#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  4 15:11:03 2019

@author: nithishreddy
"""
#import pandas as pd

import utils


price_df = utils.pd_xls_read('./data/raw_data/nse_fo_comps/price_data.xls')

price_df= price_df.iloc[:,2:]

rename_cols_2 = {
    "[NSE Symbol":'nse_symbol',
    "[Open Price": "open_price",
    "[Date": "date",
    "[Close Price": "close_price",
}

price_df.rename(columns=rename_cols_2,inplace=True)
price_df.reset_index(inplace=True,drop=True)


daywise_returns , ret_col_names = utils.make_target_open(price_df, days=list(range( 0,90 ))  , company_col='nse_symbol',
                                                       date_col = 'date')

##### make price momentum features

daywise_returns , pm_cols = utils.make_price_momentum_features(daywise_returns ,company_col='nse_symbol',
                                                        date_col='date',days = [3,7,15,30])



daywise_returns.to_pickle('./data/data_store/price_df_open_to_open.pkl')


