from sklearn.feature_selection import RFE
from sklearn.ensemble import RandomForestClassifier

def _give_sel_feat(X , y , n_f,model=RandomForestClassifier()):
    '''
    Inputs:
    X - train data y -> target
    n_f - number of features to select
    
    '''
    
    selector = RFE(model , n_features_to_select=n_f,step=10)
    selector.fit(X,y)
    return [i for i,j in zip(X.columns , selector.support_) if j==True]

