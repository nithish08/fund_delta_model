import sys
sys.path.append('../generic_tools/')
import backtest as backtest
from sklearn.preprocessing import StandardScaler
import pandas as pd
from sklearn.decomposition import PCA
import feature_selection as feat_select
import numpy as np
import utils
from sklearn import clone as clone
import pickle
from sklearn.metrics import classification_report

### TODO
#1.restructure the synthetic function
#2.Reformulate the inputs and outputs
#3. 






def train_test_returns(train,test,calc_train_stats,fl_,price_df, n_comps, n_features, 
                 gen_points, name, model,synthetic_use=False,generate_points=False,
                 signals_list=[-1, 1],threshold=1,disc2=True,time_col='date',
                 sl_rules={"target": 100, "stop_loss": -1}):
    
    X_train = train[fl_]
    X_test = test[fl_]
    y_train = train.target
#    y_test = test.target

    cols_ = X_train.columns
    scaler = StandardScaler()   # only for naive bayes
    X_train = scaler.fit_transform(X_train)
    X_test = scaler.transform(X_test)
    X_train = pd.DataFrame(X_train, columns=cols_)
    X_test = pd.DataFrame(X_test, columns=cols_)
    
    X_train_syn = X_train.copy(deep=True)
#    y_train_syn = y_train.copy()
    
    if synthetic_use:
        if generate_points:
            print('saving to the ./data/inter_data folder')
            if gen_points > 0:
                X_train, y_train = generate_random_points(
                X_train, y_train.values, no_of_points=gen_points)
    #         X_train = pd.DataFrame(X_train, columns=cols_)
            X_train = pd.DataFrame(X_train, columns=cols_)
            X_train.to_pickle('./data/inter_data/x_train_nifty200.pkl')
            with open('./data/inter_data/y_train_nifty200.pkl','wb') as f:
                pickle.dump(y_train,f)
    #         y_train.to_pickle()
            print('synthetic data saved')
    #         return None

        if not generate_points:
            print('reading from data/inter_data/ x_train and y_train')
            X_train = pd.read_pickle('./data/inter_data/x_train_nifty200.pkl')
            with open('./data/inter_data/y_train_nifty200.pkl','rb') as f:
                y_train = pickle.load(f)
    
#     print(Counter(y_train))
#     print(X_train.shape, y_train.shape)
    if n_features > 0:
        selected_features = feat_select._give_sel_feat(
            X_train, y_train, n_f=n_features, model=clone(model))
        print(selected_features)
        X_train = X_train[selected_features]
        # select only the feat select features
        X_test = X_test[selected_features]

    if n_comps > 0:
        pca = PCA(n_components=n_comps)
        print('actual', X_train.shape)
        X_train = pca.fit_transform(X_train)
        print('projected', X_train.shape)
        print("n_comps explained variance",  n_comps,
              sum(pca.explained_variance_ratio_))

        X_test = pca.transform(X_test)

    ##
    def give_row_weights(target , weight_dict):
        row_weights = target.copy()
        row_weights = list(row_weights)
        row_weights = [weight_dict[i] for i in row_weights]
        return row_weights

#     model.fit(  pd.concat( [X_train]*10 ), y_train.repeat(10) ,  \
#               sample_weight = give_row_weights(y_train.repeat(10) ,{-1:1,0:1,1:1} ) )
    model.fit( X_train , y_train   )
    print('train_complete')
    
    feature_importances_ = model.feature_importances_
    
    
    
    ## columns names are removed after PCA due to reduced dimensions
    if n_comps>0:
        preds = model.predict(X_test)
        train_preds = model.predict(X_train)
    else:
        preds = model.predict(X_test[X_train.columns])
        train_preds = model.predict(X_train_syn[X_train.columns])
    
    train_preds = model.predict(X_train)
    
    #saving train and test for checking other algos
    with open('./data/data_store/X_train.pkl','wb') as f:
        pickle.dump(X_train , f)
    with open('./data/data_store/X_test.pkl','wb') as f:
        pickle.dump(X_test , f)
    with open('./data/data_store/y_train.pkl','wb') as f:
        pickle.dump(y_train , f)
    with open('./data/data_store/y_test.pkl','wb') as f:
        pickle.dump(test.target , f)
    
    print('pickled objects saved' ) 
       
    
    print("==================train=======================" )
    print(classification_report( y_train , train_preds )  )
    print("==================test=======================" )
    print(classification_report(test.target ,preds) ) 
    
    train_df = make_signal_df(train,name,train_preds)
    test_df = make_signal_df(test,name,preds)
    
    # date, ticker, (trade_price_col), (daily_reference_price_col)
    df_prices = price_df[['nse_symbol', 'date', 'open_price', 'close_price']] ## edit for t+2 execution
    df_prices.rename(columns={'nse_symbol': 'ticker'}, inplace=True)
    df_prices.date = df_prices.date.apply(lambda x: x.date())
    
    daily_reference_price_col = "open_price" #'close_price_t2' 
    trade_price_col =  "open_price"  #'open_price_t2' # 
    
    
    
    
    test_all = backtest.get_return_day_wise(test_df, signals_list, df_prices,
                                               trade_price_col, daily_reference_price_col,sl_rules=sl_rules,
                                             portfolio_limits ={"single_order_value" : 600000, "max_exposure_value" : 5000000,
                                                               "leverage":0.5})
    
    if calc_train_stats:
        
        train_all = backtest.get_return_day_wise(train_df, signals_list, df_prices,trade_price_col,
                                                  daily_reference_price_col,sl_rules=sl_rules,
                                                 portfolio_limits ={"single_order_value" : 600000, "max_exposure_value" :                                                          5000000,"leverage":0.5} )
        
    else:
        train_all = test_all
    
    return train_all , test_all , feature_importances_ , test_df , signals_list

## new_change to see trades
    
    
    

def collate_stats(train_stats , test_stats,train_better_than_random,
                  test_better_than_random,model,name,threshold,sl_rules,
                  train_period , test_period,signals_list ,
                  n_comps, n_features,train_store_path,test_store_path,
                  train_store_name , test_store_name):
    
    #train
    train_avg_hit_rate = np.mean([i['hit_rate'] for i in train_stats])
    train_avg_hit_rate_max = np.max([i['hit_rate'] for i in train_stats])
    train_avg_hit_rate_min = np.min([i['hit_rate'] for i in train_stats])
    
    train_avg_return = np.mean([i['roi'] for i in train_stats])
    train_avg_return_max = np.max([i['roi'] for i in train_stats])
    train_avg_return_min = np.min([i['roi'] for i in train_stats])
    
    train_log_dict={}
    train_log_dict['params'] = str(model.get_params())

    train_log_dict['train_avg_hit_rate'] = train_avg_hit_rate
    train_log_dict['train_avg_hit_rate_max'] = train_avg_hit_rate_max
    train_log_dict['train_avg_hit_rate_min'] = train_avg_hit_rate_min
    
    train_log_dict['train_avg_return'] = train_avg_return
    train_log_dict['train_avg_return_max'] = train_avg_return_max
    train_log_dict['train_avg_return_min'] = train_avg_return_min
    
    train_log_dict['holding_days'] = name
    train_log_dict['stop_loss'] = sl_rules['stop_loss']
    train_log_dict['threshold'] = threshold
    train_log_dict['train_period'] = train_period
    train_log_dict['test_period'] = test_period
    
    ##add number of trades 
    
    train_log_dict['num_of_trades'] = ','.join([ str(i['trades']) for i in train_stats ])
    

    #test
    
    test_avg_hit_rate = np.mean([i['hit_rate'] for i in test_stats])
    test_avg_hit_rate_max = np.max([i['hit_rate'] for i in test_stats])
    test_avg_hit_rate_min = np.min([i['hit_rate'] for i in test_stats])
    
    test_avg_return = np.mean([i['roi'] for i in test_stats])
    test_avg_return_max = np.max([i['roi'] for i in test_stats])
    test_avg_return_min = np.min([i['roi'] for i in test_stats])
    
    test_log_dict={}
    test_log_dict['params'] = str(model.get_params())
    
    test_log_dict['test_avg_hit_rate'] = test_avg_hit_rate
    test_log_dict['test_avg_hit_rate_max'] = test_avg_hit_rate_max
    test_log_dict['test_avg_hit_rate_min'] = test_avg_hit_rate_min
    
    test_log_dict['test_avg_return'] = test_avg_return
    test_log_dict['test_avg_return_max'] = test_avg_return_max
    test_log_dict['test_avg_return_min'] = test_avg_return_min
    
    test_log_dict['holding_days'] = name
    test_log_dict['stop_loss'] = sl_rules['stop_loss']
    test_log_dict['threshold'] = threshold
    test_log_dict['train_better_than_random'] = train_better_than_random
    test_log_dict['test_better_than_random'] = test_better_than_random
    
    test_log_dict['train_period'] = train_period
    test_log_dict['test_period'] = test_period
    
    
    test_log_dict['signals_list']  = str(signals_list)
    test_log_dict['n_comps']  = n_comps
    test_log_dict['n_features']  = n_features
    
    
    test_log_dict['num_of_trades'] = ','.join([ str(i['trades']) for i in test_stats])
    
    utils.dump_dict(train_store_path , train_store_name,train_log_dict)    
    utils.dump_dict(test_store_path, test_store_name, test_log_dict)

    return train_log_dict , test_log_dict

def collate_stats_new(test_stats,model,name,threshold,sl_rules,
                  train_period , test_period,signals_list ,
                  n_comps, n_features,test_store_path,
                  test_store_name):
    #test
    
    test_avg_hit_rate = np.mean([i['hit_rate'] for i in test_stats])
    test_avg_hit_rate_max = np.max([i['hit_rate'] for i in test_stats])
    test_avg_hit_rate_min = np.min([i['hit_rate'] for i in test_stats])
    
    test_avg_return = np.mean([i['roi'] for i in test_stats])
    test_avg_return_max = np.max([i['roi'] for i in test_stats])
    test_avg_return_min = np.min([i['roi'] for i in test_stats])
    
    test_log_dict={}
    test_log_dict['params'] = str(model.get_params())
    
    test_log_dict['test_avg_hit_rate'] = test_avg_hit_rate
    test_log_dict['test_avg_hit_rate_max'] = test_avg_hit_rate_max
    test_log_dict['test_avg_hit_rate_min'] = test_avg_hit_rate_min
    
    test_log_dict['test_avg_return'] = test_avg_return
    test_log_dict['test_avg_return_max'] = test_avg_return_max
    test_log_dict['test_avg_return_min'] = test_avg_return_min
    
    test_log_dict['holding_days'] = name
    test_log_dict['stop_loss'] = sl_rules['stop_loss']
    test_log_dict['threshold'] = threshold
#    test_log_dict['train_better_than_random'] = train_better_than_random
#    test_log_dict['test_better_than_random'] = test_better_than_random
    
    test_log_dict['train_period'] = train_period
    test_log_dict['test_period'] = test_period
    
    
    test_log_dict['signals_list']  = str(signals_list)
    test_log_dict['n_comps']  = n_comps
    test_log_dict['n_features']  = n_features
    
    
    test_log_dict['num_of_trades'] = ','.join([ str(i['trades']) for i in test_stats])
    
#    utils.dump_dict(train_store_path , train_store_name,train_log_dict)    
    utils.dump_dict(test_store_path, test_store_name, test_log_dict)

    return  test_log_dict

    
def make_signal_df(test,name,preds):
    signal_df = test[['date']]
#     signal_df.rename(columns={'date': 'datetime'}, inplace=True)
    signal_df['date'] = pd.to_datetime(signal_df['date'])
    signal_df['date'] = signal_df['date'].apply(lambda x: x.date())
    signal_df['signal'] = preds
#     diff_ = int(name.split('_')[1])
    col_name_ = 'exit_date'#+str(diff_)
    signal_df['sq_off_date'] = test[col_name_]
    signal_df['sq_off_date'] = pd.to_datetime(signal_df['sq_off_date'])
    signal_df['sq_off_date'] = signal_df['sq_off_date'].apply(
        lambda x: x.date())
    
    #edit for exiting in one day
#    signal_df['sq_off_date'] = signal_df['date'].apply(
#        lambda x: x + datetime.timedelta(1)  )
#    

    signal_df['ticker'] = test.nse_symbol

#     signal_df.drop(columns='datetime', axis=1, inplace=True)
    
    signal_df.date = pd.to_datetime(signal_df.date)
    signal_df.sq_off_date = pd.to_datetime(signal_df.sq_off_date)
    signal_df.date = signal_df.date.apply(lambda x: x.date())
    signal_df.sq_off_date = signal_df.sq_off_date.apply(lambda x: x.date())
    

    return signal_df

 

def generate_random_points(data, data_labels, no_of_points):
    
    standard_scaler = StandardScaler()
    data_transformed =  standard_scaler.fit_transform(data)
    
    new_data = []
    new_data_labels = []
    
    for i in range(no_of_points):
        j = min(int(np.random.rand()*len(data_transformed)), len(data_transformed) - 1)
        temp = []
        for l in range(len(data_transformed[j])):
#             sign = np.random.rand()
            temp.append(data_transformed[j][l] + (-1+2*np.random.rand())*0.1*np.sqrt(standard_scaler.var_[l])) 
        new_data.append(temp)
        new_data_labels.append(data_labels[j])
        
    data_transformed = np.concatenate((data_transformed, new_data))
    data_original_space = standard_scaler.inverse_transform(data_transformed)
    data_all_labels = np.concatenate((data_labels, new_data_labels))
    
    return data_original_space, data_all_labels
