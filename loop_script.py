

import pandas as pd
import numpy as np
from xgboost import XGBClassifier
import utils
pd.set_option('chained_assignment',None)
import sliding_cv
import helpers
from multiprocessing import Pool
import argparse
import ast
import sys
sys.path.append('../generic_tools/')

import create_target_classes



def sliding_model_returns(param_dict):
    
    ## for the multiprocessing pool process
    
    total_all = param_dict['total_all'] #1
    train_period = param_dict['train_period']
    test_period = param_dict['test_period']
    calc_train_stats = param_dict['calc_train_stats']
    price_df = param_dict['price_df'] #5 
    fl_ = param_dict['fl_']
    n_comps = param_dict['n_comps']
    n_features = param_dict['n_features']
    gen_points = param_dict['gen_points']
    name = param_dict['name']  #10
    model = param_dict['model']
    synthetic_use = param_dict['synthetic_use']
    generate_points = param_dict['generate_points']  # just to check if its improving the hit_rate
    disc2 = param_dict['disc2']
    threshold = param_dict['threshold']  #15
    time_col = param_dict['time_col']
    sl_rules = param_dict['sl_rules']
    train_store_path = param_dict['train_store_path']
    test_store_path = param_dict['test_store_path']
    train_store_name = param_dict['train_store_name'] #20
    test_store_name = param_dict['test_store_name']  #21
    
    
    
    total = total_all.copy(deep=True)
    
    total['date'] = pd.to_datetime(total['date'])
    
    if disc2:
        total['target'] = utils.disc_2_using_thres(total[name].values, threshold=threshold) 
        signals_list = [1]
    else:
#         total['target'] = utils.disc_3_using_thres(total[name].values, threshold=threshold)
        out_df = create_target_classes.make_target_classes(price_df.iloc[:,:4]
                                ,col='open_price',threshold=threshold,
                               sq_off_days=name,company_col='nse_symbol', date_col = 'date')
    
        out_df.rename(columns={'date':'date_new'} , inplace=True)
        total = pd.merge_asof(total.sort_values('date'),out_df.sort_values('date_new'),
                                   left_on='date',right_on='date_new' , by='nse_symbol',
                    direction='forward')
        print(total.columns)
        total['target'] = total['target_class']
        signals_list = [-1,1]
    
#     print(total.shape , 'before dropping the na target and na exit_date')
    total = total.dropna(subset=['target','exit_date'] )  # dropping rows where target is np.nan
#     print(total.shape , 'after dropping the na target')
    
    sld_inds = sliding_cv.sliding_splits(total.date , train_period,test_period)
    

    training_stats = []
    testing_stats = []
    train_better_than_random_list = []
    test_better_than_random_list = []
    

    test_all_list = []
    
    feature_importances_list = []
    
    for tp in sld_inds:
        train = total.iloc[tp[0]]
        test = total.iloc[tp[1]]
        print(train.shape , test.shape)
        print('train_test enter')
        _,test_all, feat_imp  =  helpers.train_test_returns(train,test,calc_train_stats
                ,fl_,price_df, n_comps, n_features,gen_points, name, model,synthetic_use,generate_points,
                signals_list, threshold,disc2,time_col, sl_rules)
        print('train_test  complete' )
        feature_importances_list.append(feat_imp)
        test_all_list.append(test_all)
#         test_trades_df_list.append(test_trades_df)
        tr_s = test_all[2]
        te_s = test_all[2]
        training_stats.append(tr_s)
        testing_stats.append(te_s)
        
        train_better_than_random = test_better_than_random =0 
        train_better_than_random_list.append(train_better_than_random)
        test_better_than_random_list.append(test_better_than_random)
        
        
    train_better_than_random_list = np.mean(train_better_than_random_list)
    test_better_than_random_list = np.mean(test_better_than_random_list)
    
    print('logging to the train and test store files')
    
    if calc_train_stats:
        helpers.collate_stats_new(training_stats,model,name,threshold,sl_rules,
                  train_period , test_period,signals_list ,
                  n_comps, n_features,train_store_path,
                  train_store_name)
    
    helpers.collate_stats_new(testing_stats,model,name,threshold,sl_rules,
                  train_period , test_period,signals_list ,
                  n_comps, n_features,test_store_path,
                  test_store_name)
    
    print('logging done')
    
    return training_stats , testing_stats , test_all_list , feature_importances_list



def para_main(sample_) :



    model_here = XGBClassifier(max_depth=sample_['max_depth'], learning_rate=sample_['learning_rate'], 
                               n_estimators=sample_['n_estimators'], silent=True, 
                               objective='binary:logistic', booster='gbtree',
                                n_jobs=1, nthread=None, gamma=sample_['gamma'], min_child_weight=1,
                               max_delta_step=0,subsample=1, 
                               colsample_bytree=1, colsample_bylevel=1, reg_alpha=0, 
                                reg_lambda=1, scale_pos_weight= sample_['scale_pos_weight'] , 
                               base_score=0.5, random_state=0, 
                               seed=None,missing=np.nan)

    param_dict_default = {'total_all':total_all_with_zero,
        'train_period':1000 ,
        'test_period':300,
        'calc_train_stats':False  ,
        'price_df': price_df, #5
        'fl_': all_features,
        'n_comps': -1 ,
        'n_features': -1, 
        'gen_points':5000,
        'name': 5 ,  #10
        'model' : model_here,
        'synthetic_use':False,
        'generate_points':False,
        'disc2': True, 
        'threshold' : 1,
        'time_col' : 'date', #16
        'sl_rules' : {"target": sample_['threshold'], "stop_loss": sample_['stop_loss']},
        'train_store_path' : './reports/train_multi/',
        'test_store_path' : './reports/test_multi/' ,
        'train_store_name': 'traincheck'  ,  # works if as default param
        'test_store_name':  'test_check' } #21

    #modify param_dict_default into sliding_model_returns
    for i in param_dict_default:
        if i in sample_:
            param_dict_default[i] = sample_[i]



    o1,o2,o3,feature_importances_list= sliding_model_returns(param_dict=param_dict_default)

    print('complete')
    
    return o1,o2,o3 , feature_importances_list
        

def give_data():

    total_all_with_zero = pd.read_pickle('./data/data_store/total_all_open_to_open.pkl') # t+2 dataframe
    price_df = pd.read_pickle('./data/data_store/price_df_open_to_open.pkl')
    all_features = pd.read_pickle('./data/data_store/all_features.pkl')
    
    
    ## remove the earnings announcement date and consider t+1 as the 'date column'
    total_all_with_zero.drop(columns = ['date_x'] , inplace=True)
    total_all_with_zero.rename(columns={'date_y':'date'} , inplace=True)
    
    
    total_all_with_zero.nse_symbol.nunique()
#    all_features_with_momentum = all_features + ['price_mom_3','price_mom_7','price_mom_15','price_mom_30']
#    new_price_df = price_df[price_df.nse_symbol.isin(total_all_with_zero.nse_symbol.unique())]
    
    ret_cols = [i for i in total_all_with_zero.columns if i.startswith('ret_')]
    exit_date_cols = [i for i in total_all_with_zero.columns if i.startswith('exit_date')]

    total_all_with_zero.drop(columns=ret_cols+exit_date_cols,inplace=True)

    total_all_with_zero.drop(columns=['open_price','close_price','weekday'],inplace=True)

    total_all_with_zero.dropna(subset=['date'],inplace=True)
    
    
    return total_all_with_zero , all_features , price_df
    

if __name__=="__main__":

    
    
    # read data
    
    total_all_with_zero , all_features , price_df = give_data()
    
    parser = argparse.ArgumentParser(description='all arguments for delta_model')
    
    parser.add_argument('--train_period', help='train_period', default="[ 365 , 750]")
    parser.add_argument('--test_period', help='test_period', default="[90 , 180]")
    parser.add_argument('--disc2', help='disc2', default="[ False ,True]")
    parser.add_argument('--stop_loss', help='stop_loss', default="[-1]")
    parser.add_argument('--threshold', help='threshold', default="[1]")
    parser.add_argument('--gen_points', help='gen_points', default="[5000]")
    parser.add_argument('--synthetic_use', help='synthetic_use', default="[False]" )
    parser.add_argument('--generate_points', help='generate_points', default="[False]")
    parser.add_argument('--name', help='name', default="[4,7,15]")
    parser.add_argument('--max_depth', help='max_depth', default= "[3,5,7]" )
    parser.add_argument('--learning_rate', help='learning_rate', default="[0.1]")
    parser.add_argument('--n_estimators', help='n_estimators', default="[100,200]")
    parser.add_argument('--gamma', help='gamma', default="[0.1,0.03]")
    parser.add_argument('--scale_pos_weight', help='scale_pos_weight', default="[1]")
    parser.add_argument('--train_store_name', help='train_store_name', default='manual_train')
    parser.add_argument('--train_store_path', help='train_store_path', default= './reports/price_mom_train/')
    parser.add_argument('--test_store_path', help='test_store_path', default= './reports/price_mom_test/' )
    parser.add_argument('--test_store_name', help='test_store_name', default='manual_test')
    
    args = vars(parser.parse_args()) 
    
    format_d = dict((i,j) for i,j in args.items() if i not in ['train_store_name','train_store_path','test_store_path', \
                                                              'test_store_name'] )
   
    format_d = dict((i,ast.literal_eval(str(j))) for i,j in format_d.items() )
    
    args.update(format_d)
    
    for i in ['train_store_name','train_store_path','test_store_path', \
                                                              'test_store_name']:
        args[i] = [args[i]]
    
    
#     args['name'] = ['ret_'+str(i) for i in args['name'] ] 
    
    
    loop_dict = {

    'train_period': args['train_period'] ,
    'test_period': args['test_period'] ,
    'disc2': args['disc2']  ,
    'stop_loss': args['stop_loss'] ,
    'threshold':  args['threshold'] ,
    'gen_points':  args['gen_points']  ,
    'synthetic_use':args['synthetic_use'] ,
    'generate_points':args['generate_points'] ,
    'name': args['name'] ,
    'max_depth': args['max_depth'] ,
    'learning_rate': args['learning_rate'] ,
    'n_estimators': args['n_estimators'] ,
    'gamma': args['gamma'] ,
    'scale_pos_weight': args['scale_pos_weight']  ,
    'train_store_name': args['train_store_name'] ,
    'train_store_path': args['train_store_path']   ,
    'test_store_path':  args['test_store_path']   ,
    'test_store_name':  args['test_store_name']
   
    }
  
    utils.create_dir(loop_dict['train_store_path'][0])
    utils.create_dir(loop_dict['test_store_path'][0])
      
    all_iters = utils.make_dict_iterations(loop_dict)
    
    print(all_iters[0])
    #exit()
    
    for num_ in range(len(all_iters)):
        all_iters[num_]['train_store_name'] = 'train_'+str(num_)
        all_iters[num_]['test_store_name'] = 'test_'+str(num_)
    
    pool = Pool(10)
    
    pool.map(para_main , all_iters)


#
