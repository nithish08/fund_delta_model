

import pandas as pd
import numpy as np
from xgboost import XGBClassifier
import utils
pd.set_option('chained_assignment',None)
import sliding_cv
import helpers
from multiprocessing import Pool
import datetime
import statistics



#TODO:
#1. Go Through the flow
#2. Edit functions
#3. Simply to reading.



# ]:

total_all_with_zero = pd.read_pickle('./data/data_store/total_all_with_zero_with_t2.pkl') # t+2 dataframe
price_df = pd.read_pickle('./data/data_store/price_df_final_with_t2.pkl')
all_features = pd.read_pickle('./data/data_store/all_features.pkl')


## remove the earnings announcement date and consider t+1 as the 'date column'
total_all_with_zero.drop(columns = ['date_x'] , inplace=True)
total_all_with_zero.rename(columns={'date_y':'date'} , inplace=True)


#total_all_with_zero = total_all_with_zero[total_all_with_zero.nse_sym]


def sliding_model_returns(param_dict):
    
    ## for the multiprocessing pool process
    
    total_all = param_dict['total_all'] #1
    train_period = param_dict['train_period']
    test_period = param_dict['test_period']
    calc_train_stats = param_dict['calc_train_stats']
    price_df = param_dict['price_df'] #5 
    fl_ = param_dict['fl_']
    n_comps = param_dict['n_comps']
    n_features = param_dict['n_features']
    gen_points = param_dict['gen_points']
    name = param_dict['name']  #10
    model = param_dict['model']
    synthetic_use = param_dict['synthetic_use']
    generate_points = param_dict['generate_points']  # just to check if its improving the hit_rate
    disc2 = param_dict['disc2']
    threshold = param_dict['threshold']  #15
    time_col = param_dict['time_col']
    sl_rules = param_dict['sl_rules']
    train_store_path = param_dict['train_store_path']
    test_store_path = param_dict['test_store_path']
    train_store_name = param_dict['train_store_name'] #20
    test_store_name = param_dict['test_store_name']  #21
    
    
    
    total = total_all.copy(deep=True)
    
    total['date'] = pd.to_datetime(total['date'])
    
    if disc2:
        total['target'] = utils.disc_2_using_thres(total[name].values, threshold=threshold) 
        signals_list = [1]
    else:
        total['target'] = utils.disc_3_using_thres(total[name].values, threshold=threshold)
        signals_list = [-1,1]
    
#     print(total.shape , 'before dropping the na target and na exit_date')
    total = total.dropna(subset=[name,'exit_date'+str( name.split('_')[1]  )] )  # dropping rows where target is np.nan
#     print(total.shape , 'after dropping the na target')
    
    sld_inds = sliding_cv.sliding_splits(total.date , train_period,test_period)
    

    training_stats = []
    testing_stats = []
    train_better_than_random_list = []
    test_better_than_random_list = []
    

    test_all_list = []
    
    feature_importances_list = []
    
    for tp in sld_inds:
        train = total.iloc[tp[0]]
        test = total.iloc[tp[1]]
        print(train.shape , test.shape)
        print('train_test enter')
        _,test_all, feat_imp  =  helpers.train_test_returns(train,test,calc_train_stats
                ,fl_,price_df, n_comps, n_features,gen_points, name, model,synthetic_use,generate_points,
                signals_list, threshold,disc2,time_col, sl_rules)
        print('train_test  complete' )
        feature_importances_list.append(feat_imp)
        test_all_list.append(test_all)
#         test_trades_df_list.append(test_trades_df)
        tr_s = test_all[2]
        te_s = test_all[2]
        training_stats.append(tr_s)
        testing_stats.append(te_s)
        
        train_better_than_random = test_better_than_random =0 
        train_better_than_random_list.append(train_better_than_random)
        test_better_than_random_list.append(test_better_than_random)
        
        
    train_better_than_random_list = np.mean(train_better_than_random_list)
    test_better_than_random_list = np.mean(test_better_than_random_list)
    
    print('logging to the train and test store files')
    
    if calc_train_stats:
        helpers.collate_stats_new(training_stats,model,name,threshold,sl_rules,
                  train_period , test_period,signals_list ,
                  n_comps, n_features,train_store_path,
                  train_store_name)
    
    helpers.collate_stats_new(testing_stats,model,name,threshold,sl_rules,
                  train_period , test_period,signals_list ,
                  n_comps, n_features,test_store_path,
                  test_store_name)
    
    print('logging done')
    
    return training_stats , testing_stats , test_all_list , feature_importances_list



def para_main(sample_) :



    model_here = XGBClassifier(max_depth=sample_['max_depth'], learning_rate=sample_['learning_rate'], 
                               n_estimators=sample_['n_estimators'], silent=True, 
                               objective='binary:logistic', booster='gbtree',
                                n_jobs=1, nthread=None, gamma=sample_['gamma'], min_child_weight=1,
                               max_delta_step=0,subsample=1, 
                               colsample_bytree=1, colsample_bylevel=1, reg_alpha=0, 
                                reg_lambda=1, scale_pos_weight= sample_['scale_pos_weight'] , 
                               base_score=0.5, random_state=0, 
                               seed=None,missing=np.nan)

    param_dict_default = {'total_all':total_all_with_zero,
        'train_period':1000 ,
        'test_period':300,
        'calc_train_stats':False  ,
        'price_df': price_df, #5
        'fl_': all_features,
        'n_comps': -1 ,
        'n_features': -1, 
        'gen_points':5000,
        'name': 'ret_7',  #10
        'model' : model_here,
        'synthetic_use':False,
        'generate_points':False,
        'disc2': True, 
        'threshold' : 1,
        'time_col' : 'date', #16
        'sl_rules' : {"target": 100, "stop_loss": sample_['stop_loss']},
        'train_store_path' : './reports/train_multi/',
        'test_store_path' : './reports/test_multi/' ,
        'train_store_name': 'traincheck'  ,  # works if as default param
        'test_store_name':  'test_check' } #21

    #modify param_dict_default into sliding_model_returns
    for i in param_dict_default:
        if i in sample_:
            param_dict_default[i] = sample_[i]



    o1,o2,o3,feature_importances_list= sliding_model_returns(param_dict=param_dict_default)

    print('complete')
    
    return o1,o2,o3 , feature_importances_list
        


# ]:



total_all_with_zero.nse_symbol.nunique()
all_features_with_momentum = all_features + ['price_mom_3','price_mom_7','price_mom_15','price_mom_30']
new_price_df = price_df[price_df.nse_symbol.isin(total_all_with_zero.nse_symbol.unique())]



## remove the earnings announcement date and consider t+1 as the 'date column'
#total_all_with_zero.drop(columns = ['date_x'] , inplace=True)
#total_all_with_zero.rename(columns={'date_y':'date'} , inplace=True)

#
#loop_dict = {
#
#    'train_period':[ 365 , 750],
#    'test_period':[90 , 180],
#    'disc2':[ False ,True],
#    'stop_loss':[-1],
#    'threshold':[1],
#    'fl_': [all_features_with_momentum],
#    'gen_points':[5000],
#    'synthetic_use':[False],
#    'generate_points':[False],
#    'name':['ret_4','ret_7','ret_15'],
#    'max_depth':[3,5,7],
#    'learning_rate':[0.1],
#    'n_estimators':[100,200],
#    'gamma':[0.1,0.03],
#    'scale_pos_weight':[1],
#    'train_store_name':['manual_train'],
#    'train_store_path':['./reports/price_mom_train/'],
#    'test_store_path':['./reports/price_mom_test/'],
#    'test_store_name':['manual_test' ]
#    }



loop_dict = {

    'train_period': 700,
    'test_period':180,
    'disc2':False,
    'stop_loss':-1,
    'threshold':1,
    'fl_': all_features,
    'gen_points':5000,
    'synthetic_use':False,
    'generate_points':False,
    'name':'ret_7',
    'max_depth':7,
    'learning_rate':0.1,
    'n_estimators':100,
    'gamma':0.1,
    'scale_pos_weight':1,
    'train_store_name':'manual_train',
    'train_store_path':'./reports/price_mom_train/',
    'test_store_path':'./reports/price_mom_test/',
    'test_store_name':'manual_test' 
    }


o1,o2,o3 , feature_importances_list = para_main(loop_dict)

#from statistics import all_signal_ratios

#stats_df = all_signal_ratios(o3)




df_prices_nifty = pd.read_csv('./data/nifty100_2018_2_1.csv')
df_prices_nifty["datetime"] = df_prices_nifty['Date'].apply(lambda xx:  datetime.datetime.strptime(xx , '%Y-%m-%d' ).date() )
df_prices_nifty = df_prices_nifty.sort_values(by=['datetime'])
df_prices_nifty.datetime = pd.to_datetime(df_prices_nifty.datetime)
df_prices_nifty.datetime = df_prices_nifty.datetime.apply(lambda x: x.date())




statistics.simple_result(o3,df_prices_nifty ,'./reports/t2_train_test.xlsx')

import statistics

x = statistics.static_capital_leverage(o3,df_prices_nifty)

#
#all_iters = utils.make_dict_iterations(loop_dict)
#
#
#for num_ in range(len(all_iters)):
#    all_iters[num_]['train_store_name'] = 'train_'+str(num_)
#    all_iters[num_]['test_store_name'] = 'test_'+str(num_)
#
#pool = Pool(2)
#
#pool.map(para_main , all_iters)
#
#
#
#len(all_iters)

